"""RipioPruebaTecnica URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from drf_yasg2.views import get_schema_view
from drf_yasg2 import openapi
from rest_framework import permissions
from apps.users.api.views import LoginAPIView
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token

admin.site.site_header = 'Administración de Ripio'
admin.site.site_title = 'Administración de Ripio'
admin.site.index_title = 'Sección de administración de Ripio'

schema_view = get_schema_view(
   openapi.Info(
      title="Ripio API",
      default_version='v1',
      description="API del proyecto Ripio",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="lesz0.z3r0@gmail.com"),
      license=openapi.License(name="Software Propietario"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    # API Docs
    path('api/docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('api/redocs/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    # Admin URL
    path('admin/', admin.site.urls),

    # AUTH URLS
    path('api/auth/login', LoginAPIView.as_view(), name='auth_login'),
    path('api/auth/renew-token', refresh_jwt_token, name='auth_renew_token'),
    path('api/auth/verify-token', verify_jwt_token, name='auth_verify_token'),

    # APPS URLS
    path('api/users', include("apps.users.api.urls", namespace="users_api")),
    path('api/currencies', include("apps.currencies.api.urls", namespace="currencies_api")),
]