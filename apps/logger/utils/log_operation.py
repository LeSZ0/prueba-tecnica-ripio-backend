from apps.logger.models import Logger
import json


def logger(data: dict) -> None:
    Logger.objects.create(
        operation=data['operation'],
        detail=data['detail'],
        user_id=data['user']
    )
