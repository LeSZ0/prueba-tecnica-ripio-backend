from django.db import models


class Logger(models.Model):
    operation = models.CharField(verbose_name='Operation', max_length=150)
    detail = models.CharField(verbose_name='Detail', max_length=500)

    user = models.ForeignKey(
        to="users.User", verbose_name='Usuario', on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(
        verbose_name='Operation date', auto_now_add=True
    )

    class Meta:
        verbose_name = 'Activity Log'
        verbose_name_plural = 'Activity Logs'
        ordering = ['-created_at']
        db_table = 'logger_logger'

    def __str__(self) -> str:
        return f'{self.operation}, {self.created_at.strftime("%d/%m/%y")}'
