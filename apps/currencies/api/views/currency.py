from rest_framework import generics, permissions, status
from rest_framework.response import Response
from apps.currencies.models import Currency
from typing import Callable
import apps.currencies.api.serializers as currencies_serializers
from utils import StandardPagination


class CurrencyListCreateAPIView(generics.GenericAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    queryset = Currency.objects.filter(is_active=True)
    pagination_class = StandardPagination

    def get_serializer_class(self) -> Callable:
        return {
            'GET': lambda: currencies_serializers.CurrencyListSerializer,
            'POST': lambda: currencies_serializers.CurrencyCreateSerializer
        }.get(self.request.method, lambda: currencies_serializers.CurrencyListSerializer)()

    def get(self, request, *args, **kwargs) -> Response:
        """Currencies list

        This endpoint returns a list of all active currencies.
        """
        currencies = self.get_queryset()

        # Currencies list through paginator
        page = self.paginate_queryset(currencies)

        if page is not None:
            # Serialize and return data from paginator
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        # If don't exist any entry, return data without paginate
        serializer = self.get_serializer(currencies, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs) -> Response:
        """Create currency

        This endpoint allows to create a currency.
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            data = serializer.data
            currency = Currency.objects.create(
                name=data['name'],
                short_name=data['short_name'],
                existence=data['existence'] if data['existence'] else None
            )

            detail_serializer = currencies_serializers.CurrencyDetailSerializer(currency)

            return Response(
                detail_serializer.data, status=status.HTTP_201_CREATED
            )

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CurrencyRetrieveUpdateDeleteAPIView(generics.GenericAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
        permissions.IsAdminUser
    ]
    queryset = Currency.objects.all()

    def get_serializer_class(self) -> Callable:
        return {
            'GET': lambda: currencies_serializers.CurrencyDetailSerializer,
            'PUT': lambda: currencies_serializers.CurrencyUpdateSerializer
        }.get(
            self.request.method,
            lambda: currencies_serializers.CurrencyDetailSerializer
        )()

    def get(self, request, *args, **kwargs) -> Response:
        """Currency detail

        This endpoint returns the detail of the currency.
        """
        currency_id = kwargs['pk']
        currency = Currency.objects.get(id=currency_id)
        serializer = self.get_serializer(currency)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs) -> Response:
        """Update currency

        This endpoint allows to update the currency.
        """
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            data = serializer.data
            currency_id = kwargs['pk']

            try:
                currency = Currency.objects.get(id=currency_id)
                currency.name = data['name']
                currency.short_name = data['short_name']
                
                currency.save(update_fields=['name', 'short_name'])

                detail_serializer = currencies_serializers.CurrencyDetailSerializer(currency)

                return Response(detail_serializer.data, status=status.HTTP_200_OK)

            except Currency.DoesNotExist:
                return Response(
                    'Currency does not exist', status=status.HTTP_404_NOT_FOUND
                )

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs) -> Response:
        """Delete currency

        This endpoint allows to delete a currency. <br />
        The deletion will be a logic deletion using the <i>is_active</i> field.
        """
        currency_id = kwargs['pk']

        try:
            currency = Currency.objects.get(id=currency_id)
            currency.is_active = False
            currency.save(update_fields=['is_active'])

            return Response(
                'Currency deleted successfully', status=status.HTTP_200_OK
            )

        except Currency.DoesNotExist:
            return Response(
                'Currency does not exist', status=status.HTTP_404_NOT_FOUND
            )
