from django.urls import path
import apps.currencies.api.views as currencies_views


app_name = 'currencies_api'
urlpatterns = [
    path(
        '', currencies_views.CurrencyListCreateAPIView.as_view(),
        name='currency_list_create'
    ),
    path(
        '/<int:pk>', currencies_views.CurrencyRetrieveUpdateDeleteAPIView.as_view(),
        name='currency_retrieve_update_destroy'
    )
]