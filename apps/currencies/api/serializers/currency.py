from rest_framework import serializers
from apps.currencies.models import Currency


class CurrencyListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = [
            'id', 'name', 'short_name', 'existence'
        ]
        read_only_fields = [
            'id', 'name', 'short_name', 'existence'
        ]


class CurrencyCreateSerializer(serializers.ModelSerializer):
    existence = serializers.FloatField(allow_null=True, default=None)

    class Meta:
        model = Currency
        fields = [
            'name', 'short_name', 'existence'
        ]


class CurrencyUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = [
            'name', 'short_name'
        ]


class CurrencyDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = [
            'id', 'name', 'short_name', 'existence', 'is_active'
        ]
        read_only_fields = [
            'id', 'name', 'short_name', 'existence', 'is_active'
        ]
