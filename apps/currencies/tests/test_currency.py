from django.test import TestCase
from apps.currencies.models import Currency
from apps.users.models import User
from django.urls import reverse


class CurrencyTests(TestCase):
    def setUp(self):
        User.objects.create_user(
            username='user_test',
            email='user@test.com',
            full_name='Test User',
            password='123456'
        )
        Currency.objects.create(
            name='American Dollar',
            short_name='USD'
        )

    def test_create_currency(self):
        self.client.login(username='user_test', password='123456')
        data = {
            'name': 'Pesos Argentinos',
            'short_name': 'ARS'
        }
        resp = self.client.post(
            reverse('currencies_api:currency_list_create'),
            data=data
        )
        self.assertEqual(resp.status_code, 201)

    def test_list_currencies(self):
        self.client.login(username='user_test', password='123456')
        resp = self.client.get(
            reverse('currencies_api:currency_list_create')
        )
        self.assertEqual(resp.status_code, 200)

    def test_only_admin_user_can_update_destroy_detail_currency(self):
        self.client.login(username='user_test', password='123456')
        currency = Currency.objects.get(short_name='USD')
        resp = self.client.get(reverse(
            'currencies_api:currency_retrieve_update_destroy',
            kwargs={'pk': currency.id}
        ))
        self.assertEqual(resp.status_code, 403)

    def test_detail_currency(self):
        User.objects.create_superuser(
            username='superuser', password='1234879',
            email='superuser@email.com', full_name='Superuser'
        )
        self.client.login(username='superuser', password='1234879')
        currency = Currency.objects.get(short_name='USD')
        resp = self.client.get(reverse(
            'currencies_api:currency_retrieve_update_destroy',
            kwargs={'pk': currency.id}
        ))
        self.assertEqual(resp.status_code, 200)

    def test_currencies_list_paginated(self):
        self.client.login(username='user_test', password='123456')
        resp = self.client.get(
            reverse('currencies_api:currency_list_create')
        )
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.data['count'], 1)
