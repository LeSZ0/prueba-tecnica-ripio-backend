from django.contrib import admin
from apps.currencies.models import Currency


admin.site.register(Currency)
