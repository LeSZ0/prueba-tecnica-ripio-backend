from django.db import models


class Currency(models.Model):
    name = models.CharField(verbose_name='Name', max_length=150)
    short_name = models.CharField(verbose_name='Short name', max_length=3)
    existence = models.FloatField(verbose_name='Existence', null=True, blank=True)
    is_active = models.BooleanField(verbose_name='Is active?', default=True)

    class Meta:
        verbose_name = 'Currency'
        verbose_name_plural = 'Currencies'
        ordering = ['-name']
        db_table = 'currencies_currency'

    def __str__(self) -> str:
        return self.short_name
