from rest_framework import serializers
from apps.users.models import UserCurrency
from apps.currencies.api.serializers import CurrencyDetailSerializer


class UserCurrencyTransferSerializer(serializers.Serializer):
    target = serializers.CharField()
    currency = serializers.IntegerField()
    amount = serializers.FloatField()


class UserCurrencyBalanceSerializer(serializers.ModelSerializer):
    currency = CurrencyDetailSerializer()

    class Meta:
        model = UserCurrency
        fields = [
            'currency', 'amount', 'created_at',
            'updated_at'
        ]
        read_only_fields = [
            'currency', 'amount', 'created_at',
            'updated_at'
        ]


class UserCurrencyAddToBalanceSerializer(serializers.ModelSerializer):
    currency = serializers.IntegerField()

    class Meta:
        model = UserCurrency
        fields = [
            'currency', 'amount'
        ]
