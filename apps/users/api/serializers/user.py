from rest_framework import serializers
from apps.users.models import User


class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id', 'username', 'full_name',
            'email', 'is_active'
        ]
        read_only_fields = [
            'id', 'username', 'full_name',
            'email', 'is_active'
        ]


class UserCreateSerializer(serializers.ModelSerializer):
    password = serializers.CharField()

    class Meta:
        model = User
        fields = [
            'username', 'full_name', 'email', 'password'
        ]


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id', 'username', 'full_name', 'email'
        ]
        read_only_fields = [
            'id', 'username', 'full_name', 'email'
        ]
