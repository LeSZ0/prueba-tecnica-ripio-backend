from django.urls import path
import apps.users.api.views as users_views


app_name = 'users_api'
urlpatterns = [
    path('', users_views.UserListCreateAPIView.as_view(), name='user_list_create'),
    path(
        '/currencies/balance', users_views.UserCurrencyBalanceAPIView.as_view(),
        name='user_currency_balance'
    ),
    path(
        '/currencies', users_views.UserTransferCurrencyAPIView.as_view(),
        name='user_currency_transfer'
    ),
]