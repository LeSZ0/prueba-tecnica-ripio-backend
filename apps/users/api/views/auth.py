from rest_framework_jwt.views import ObtainJSONWebToken
from django.contrib.auth.models import update_last_login
from rest_framework_jwt.settings import api_settings
from apps.users.models import User
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import get_user_model
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from rest_framework import generics, permissions, status
from rest_framework.response import Response
from apps.users.api.serializers import LoginSerializer
import datetime

jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER


class LoginAPIView(ObtainJSONWebToken):
    permission_classes = [permissions.AllowAny,]
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        """Login

        This endpoint allows the user to login and returns the user's token.
        """
        serializer = self.get_serializer(data=request.data, context={'request': request})

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            response_data = jwt_response_payload_handler(token, user, request)
            response = Response(response_data)
            update_last_login(None, user)

            if api_settings.JWT_AUTH_COOKIE:
                expiration = (
                    datetime.datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA
                )
                response.set_cookie(
                    api_settings.JWT_AUTH_COOKIE,
                    token,
                    expires=expiration,
                    httponly=True
                )

            return response

        else:
            try:
                return Response(
                    {'message': serializer.errors['non_field_errors'][0]},
                    status=status.HTTP_400_BAD_REQUEST
                )

            except BaseException:
                return Response({'message': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
