from rest_framework import generics, permissions, status
from rest_framework.response import Response
from apps.users.models import User
import apps.users.api.serializers as user_serializers
from typing import Callable


class UserListCreateAPIView(generics.GenericAPIView):
    queryset = User.objects.filter(is_active=True)

    def get_permission_classes(self) -> list:
        return {
            'GET': [permissions.IsAuthenticated,],
            'POST': [permissions.AllowAny,]
        }.get(self.request.method, [permissions.IsAuthenticated,])

    def get_serializer_class(self) -> Callable:
        return {
            'GET': lambda: user_serializers.UserListSerializer,
            'POST': lambda: user_serializers.UserCreateSerializer
        }.get(self.request.method, lambda: user_serializers.UserListSerializer)()

    def get(self, request, *args, **kwargs) -> Response:
        """Users list

        This endpoint returns the list of users,
        however the list will exclude superusers.
        """
        users = User.objects.filter(is_active=True).exclude(is_superuser=True)
        serializer = self.get_serializer(users, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs) -> Response:
        """Create user

        This endpoint allows to creating a normal user.
        """

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            data = serializer.data
            user = User.objects.create_user(
                username=data['username'],
                email=data['email'],
                full_name=data['full_name'],
                password=data['password']
            )

            return Response(
                user_serializers.UserDetailSerializer(user).data,
                status=status.HTTP_201_CREATED
            )

        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
