from rest_framework import generics, permissions, status
from rest_framework.response import Response
from apps.users.models import UserCurrency
from apps.users.utils import transfer_currency
from apps.currencies.models import Currency
import apps.users.api.serializers as ucurrency_serializers
from apps.logger.utils import logger


class UserCurrencyBalanceAPIView(generics.GenericAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    queryset = []

    def get_serializer_class(self) -> callable:
        return {
            'GET': lambda: ucurrency_serializers.UserCurrencyBalanceSerializer,
            'POST': lambda: ucurrency_serializers.UserCurrencyAddToBalanceSerializer
        }.get(
            self.request.method,
            lambda: ucurrency_serializers.UserCurrencyBalanceSerializer
        )()

    def get(self, request, *args, **kwargs) -> Response:
        """User currencies balance

        This endpoint returns the balance of the user for each currency.
        """
        user_id = request.user.id
        user_currencies = UserCurrency.objects.filter(user_id=user_id)
        serializer = self.get_serializer(user_currencies, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs) -> Response:
        """Add currency to balance

        This endpoint allows the user to add a sepecific currency to your balance.
        """
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            data = serializer.data
            user_id = request.user.id

            # Verify if user has the currency on your balance or not.
            try:
                user_currency = UserCurrency.objects.get(
                    user_id=user_id,
                    currency_id=data['currency']
                )
                user_currency.amount += data['amount']
                user_currency.save(update_fields=['amount'])

            except UserCurrency.DoesNotExist:
                user_currency = UserCurrency.objects.create(
                    user_id=user_id,
                    currency_id=data['currency'],
                    amount=data['amount']
                )

            currency = Currency.objects.get(id=data['currency'])

            # If currency has a limited existence, substract the amount.
            if currency.existence is not None:
                currency.existence -= data['amount']
                currency.save(update_fields=['existence'])

            # FIXME: SEND THIS OPERATION TO A CELERY TASK.

            logger_data = {
                'operation': 'Add currency to balance',
                'detail': f'{request.user.full_name} agregó {currency.short_name} {data["amount"]} a su balance.',
                'user': request.user.id
            }

            logger(logger_data)

            # END FIX

            detail_serializer = ucurrency_serializers.UserCurrencyBalanceSerializer(
                user_currency
            )

            return Response(detail_serializer.data, status=status.HTTP_200_OK)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserTransferCurrencyAPIView(generics.GenericAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    serializer_class = ucurrency_serializers.UserCurrencyTransferSerializer

    def post(self, request, *args, **kwargs) -> Response:
        """Transfer currency

        This endpoint allows the user to transfer an amount of a currency to another user.
        """
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            request_user_id = request.user.id
            data = serializer.data

            currency = Currency.objects.get(id=data['currency'])

            transference = transfer_currency(
                origin=request_user_id,
                target=serializer.data['target'],
                currency=currency,
                amount=data['amount']
            )

            if transference['status']:
                status_response = status.HTTP_200_OK

            else:
                status_response = status.HTTP_409_CONFLICT

            return Response(transference['msg'], status=status_response)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer.data, status=status.HTTP_200_OK)
