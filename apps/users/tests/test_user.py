from django.test import TestCase
from apps.users.models import User, UserCurrency
from django.urls import reverse


class UsersTests(TestCase):
    def setUp(self):
        User.objects.create_user(
            username='user_test',
            email='user@test.com',
            full_name='Test User',
            password='123456'
        )
        User.objects.create_user(
            username='user_test_two',
            email='user@test_two.com',
            full_name='Test Two User',
            password='654321'
        )

    def test_user_creation(self):
        self.client.login(username='user_test', password='123456')
        new_user_data = {
            'username': 'user_test_three',
            'full_name': 'User Three',
            'email': 'usertestthree@test.com',
            'password': '1234597'
        }
        resp = self.client.post(
            reverse('users_api:user_list_create'),
            data=new_user_data
        )
        self.assertEqual(resp.status_code, 201)

    def test_user_list(self):
        self.client.login(username='user_test', password='123456')
        resp = self.client.get(
            reverse('users_api:user_list_create')
        )
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.data), 2)
