from django.test import TestCase
from apps.users.models import User, UserCurrency
from apps.currencies.models import Currency
from django.urls import reverse


class UserCurrencyTests(TestCase):
    def setUp(self):
        user_test = User.objects.create_user(
            username='user_test',
            email='user@test.com',
            full_name='Test User',
            password='123456'
        )
        User.objects.create_user(
            username='user_test_two',
            email='user@test_two.com',
            full_name='Test Two User',
            password='654321'
        )

        usd_currency = Currency.objects.create(
            name='American Dollar',
            short_name='USD'
        )
        btc_currency = Currency.objects.create(
            name='Bitcoin',
            short_name='BTC',
            existence=21000000
        )

        UserCurrency.objects.create(
            currency=usd_currency,
            user=user_test,
            amount=50000
        )
        UserCurrency.objects.create(
            currency=btc_currency,
            user=user_test,
            amount=3
        )

    def test_user_currency_balance(self):
        self.client.login(username='user_test', password='123456')
        resp = self.client.get(
            reverse('users_api:user_currency_balance')
        )
        self.assertEqual(resp.status_code, 200)

    def test_add_currency_to_user_balance(self):
        self.client.login(username='user_test_two', password='654321')
        currency = Currency.objects.get(short_name='BTC')
        currency_data = {
            'currency': currency.id,
            'amount': 5
        }
        resp = self.client.post(
            reverse('users_api:user_currency_balance'),
            data=currency_data
        )
        self.assertEqual(resp.status_code, 200)

    def test_user_transfer_currency(self):
        self.client.login(username='user_test', password='123456')
        target = User.objects.get(username='user_test_two')
        currency = Currency.objects.get(short_name='USD')
        currency_data = {
            'currency': currency.id,
            'amount': 4000,
            'target': target.id
        }
        resp = self.client.post(
            reverse('users_api:user_currency_transfer'),
            data=currency_data
        )
        self.assertEqual(resp.status_code, 200)

    def test_user_have_enough_founds_to_transfer_currency(self):
        self.client.login(username='user_test', password='123456')
        target = User.objects.get(username='user_test_two')
        currency = Currency.objects.get(short_name='BTC')
        currency_data = {
            'currency': currency.id,
            'amount': 4000,
            'target': target.id
        }
        resp = self.client.post(
            reverse('users_api:user_currency_transfer'),
            data=currency_data
        )
        self.assertEqual(resp.status_code, 409)
        self.assertEqual(
            resp.data,
            'You do not have the amount for this currency in your balance'
        )
