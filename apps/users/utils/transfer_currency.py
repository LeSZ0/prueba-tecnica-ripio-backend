from apps.users.models import UserCurrency, User
from typing import Dict, Any
from apps.logger.utils import logger


def transfer_currency(origin: str, target: str, currency: object, amount: float) -> Dict[str, Any]:
    if str(origin) != str(target):
        try:
            origin_currency = UserCurrency.objects.get(
                user_id=origin, currency_id=currency.id
            )

        except UserCurrency.DoesNotExist:
            origin_currency = None

            return {
                'status': False,
                'msg': 'You do not have founds for this currency'
            }

        if origin_currency.amount > float(0) and origin_currency.amount >= amount:
            try:
                origin_currency.amount -= amount
                origin_currency.save(update_fields=['amount'])

                try:
                    target_currency = UserCurrency.objects.get(
                        user_id=target, currency_id=currency.id
                    )
                    target_currency.amount += amount
                    target_currency.save(update_fields=['amount'])

                except UserCurrency.DoesNotExist:
                    UserCurrency.objects.create(
                        user_id=target,
                        currency_id=currency.id,
                        amount=amount
                    )

                # FIXME: SEND THIS OPERATION TO A CELERY TASK.

                origin_user = User.objects.get(id=origin)
                target_user = User.objects.get(id=target)

                logger_data = {
                    'operation': 'Transfer currency from balance',
                    'detail': f'{origin_user.full_name} envió una transferencia de {currency.short_name} {amount} a {target_user.full_name}.',
                    'user': origin
                }

                logger(logger_data)

                # END FIX

                return {
                    'status': True,
                    'msg': f'Transferred {currency.short_name} {amount} successfully'
                }

            except BaseException:
                return {
                    'status': False,
                    'msg': 'Oops! An error was ocurred, your transference was not completed.'
                }

        else:
            return {
                'status': False,
                'msg': 'You do not have the amount for this currency in your balance'
            }

    else:
        return {
            'status': False,
            'msg': 'You cannot transfer currencies to yourself'
        }
