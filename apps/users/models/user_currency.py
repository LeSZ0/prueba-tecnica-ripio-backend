from django.db import models


class UserCurrency(models.Model):
    user = models.ForeignKey(
        to='users.User', verbose_name='User', on_delete=models.CASCADE
    )
    currency = models.ForeignKey(
        to='currencies.Currency', verbose_name='Currency', on_delete=models.CASCADE
    )
    amount = models.FloatField(verbose_name='Amount')

    created_at = models.DateTimeField(verbose_name='Created at', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='Updated at', auto_now=True)

    class Meta:
        verbose_name = 'User Currency'
        verbose_name = 'User Currencies'
        ordering = ['-created_at']
        db_table = 'users_user_currency'

    def __str__(self) -> str:
        return f'{self.user.full_name} - {self.currency.name}'
