import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
import datetime


class UserManager(BaseUserManager):
    def save_user(self, username, email, full_name, password):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            username=username,
            full_name=full_name
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email, full_name, password):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            username=username,
            full_name=full_name
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, full_name, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """

        user = self.create_user(
            email=email,
            username=username,
            full_name=full_name,
            password=password,
        )
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractUser):
    id = models.UUIDField(
        verbose_name='ID', primary_key=True, default=uuid.uuid4, editable=False
    )
    full_name = models.CharField(
        max_length=500, verbose_name='Full Name'
    )
    username = models.CharField(
        verbose_name='Username', unique=True, max_length=500
    )
    email = models.EmailField(
        verbose_name='Email', max_length=500, unique=True
    )
    is_active = models.BooleanField(
        verbose_name='Is active?', default=True,
        help_text='If is not active, then will not appear in users list'
    )

    USERNAME_FIELD = 'username'

    REQUIRED_FIELDS = ['full_name', 'email']

    objects = UserManager()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
        db_table = 'users_user'
        ordering = ['full_name']

    def __str__(self) -> str:
        return self.full_name
